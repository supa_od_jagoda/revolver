﻿using Revolver.Models;
using Revolver.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Revolver.Controllers
{
    public class HomeController : Controller
    {
        private ItemRepository itemRepository = new ItemRepository();
        private OrderSubscriptionRepository orderRepository = new OrderSubscriptionRepository();
        private StoryRepository storyRepository = new StoryRepository();


        public ActionResult Index()
        {
            //preuzimanje svih PULOVERA iz baze i smestanje u ItemsViewModel
            ItemsViewModel itemsView = new ItemsViewModel(itemRepository.GetAll());

            return View("Index", itemsView);
        }

        //otvaranje product stranice
        public ActionResult Products()
        {
            //preuzimanje svih PULOVERA iz baze i smestanje u ItemsViewModel
            ItemsViewModel itemsView = new ItemsViewModel(itemRepository.GetAll());

            return View(itemsView);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //GET akcija za otvaranje Product stranice
        [HttpGet]
        public ActionResult Product(int id)
        {
            //preuzimanje itema po ID-u
            var item = itemRepository.GetById(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            //Product stranica prima OrderItem
            var orderItem = new OrderItem();
            orderItem.ItemId = item.Id;
            orderItem.Size = ItemSize.M; // Default selekcija
            orderItem.Item = item;
            return View("Product", orderItem);
        }

        //preuzimanje slika za konkretan produkt koje nisu Thumbnail
        [HttpGet]
        public JsonResult ProductImages(int id)
        {
            var itemImages = itemRepository.GetById(id).Images.Where(img=>!img.IsThumb).
                Select(img => new { url = Url.Content(img.Url) }).ToList();
            if (itemImages.Count <= 0 && itemImages == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }

            return Json(itemImages, JsonRequestBehavior.AllowGet);
        }

        //GET akcija za otvaranje Story stranice
        [HttpGet]
        public ActionResult Story(int id)
        {
            var story = storyRepository.GetById(id);
            if (story == null)
            {
                return HttpNotFound();
            }

            return View("Story", story);
        }

        //preuzimanje slika za konkretan Story
        [HttpGet]
        public JsonResult StoryImages(int id)
        {
            var storyImages = storyRepository.GetById(id).Images.Select(img => new { url = Url.Content(img.Url) }).ToList();
            if (storyImages.Count <= 0 && storyImages == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }

            return Json(storyImages, JsonRequestBehavior.AllowGet);
        }

        //preuzimanje svih prica za true story slajder na Index stranici
        [HttpGet]
        public JsonResult GetAllStories()
        {
            var storiesAll = storyRepository.GetAll();

            var stories = storiesAll.Select(s => new
            {
                storyId = s.Id,
                storyTitle = s.Title,
                storyAuthorName = s.AuthorName,
                
                //TODO PROVERA POSTOJANJA SLIKE VEZANE ZA STORYID
                imageUrl = Url.Content(s.Images.First().Url)

            });

            return Json(stories, JsonRequestBehavior.AllowGet);
        }



        //stavljanje Item-a u korpu, potreban je itemId i i itemSize  
        [HttpPost]
        public ActionResult AddToCart(int itemId, ItemSize itemSize)
        {
            var item = itemRepository.GetById(itemId);
            if (item == null)
            {
                return HttpNotFound();
            }

            //preuzimanje ordera iz sesije
            Order order = Session["UserOrder"] as Order;
            //ako order ne postoji, u obradi je ili placen kreirati novi order
            if (order == null || order.Status == (int)OrderStatus.Pending || order.Status == (int)OrderStatus.Paid)
            {
                order = new Order();
                order.Date = DateTime.Now;
                order.OrderItems = new List<OrderItem>();
                Session["UserOrder"] = order;
            }
            //zadavanje created statusa orderu
            order.Status = (int)OrderStatus.Created;

            //provera da li isti orderItem (isti itemId i isti itemSize) postoji vec u listi orderItema
            var orderItem = order.OrderItems.FirstOrDefault(oi => oi.ItemId == itemId && oi.Size == itemSize);

            //ako ne postoji kreirati novi i dodati ga u listu orderItem-a
            if (orderItem == null)
            {
                orderItem = new OrderItem
                {
                    OrderId = order.Id,
                    ItemId = item.Id,
                    Size = itemSize,
                    Quantity = 1, // Default quantity
                    OrderPrice = item.Price,
                    Item = item
                };
                order.OrderItems.Add(orderItem);

            }
            //ko postoji treba povecati kvantitet postojeceg orderItem-a
            else
            {
                orderItem.Quantity++;
            }


            //vracamo broj itema u korpi da bih mogla da dodam broj na R
            return Json(new { CartNumber = order.OrderItems.Sum(oi => oi.Quantity) });
        }

        //preuzimanje ordera iz sesije i slanje na PartialView
        [HttpGet]
        public ActionResult OpenCart()
        {
            Order order = Session["UserOrder"] as Order;

            return PartialView("CartOrderPopup", order);

        }

        //checkout cart-a i otvaranje confirmation stranice
        [HttpPost]
        public ActionResult Checkout(Order order)
        {

            var dbOrder = Session["UserOrder"] as Order;
            if (dbOrder == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                order.Date = dbOrder.Date;
                float discount = 0.0f;

                //provera da li postoji subscription da bi discount code bio validan
                if (!string.IsNullOrEmpty(order.DiscountCode))
                {
                    var subsciption = orderRepository.GetSubscriptionByEmail(order.CustomerEmail);
                    if (subsciption != null)
                    {
                        if (subsciption.DiscountCode.Code == order.DiscountCode && !subsciption.DiscountCode.Used)
                        {
                            order.DiscountCodeId = subsciption.DiscountCodeId;
                            discount = subsciption.DiscountCode.Value / 100;
                        }
                    }
                    else
                    {
                        // TODO: Handle non existant subscription.
                    }
                }

                float totalPrice = 0.0f;
                order.OrderItems = new List<OrderItem>();
                foreach (OrderItem orderItem in dbOrder.OrderItems)
                {
                    order.OrderItems.Add(new OrderItem
                    {
                        Id = orderItem.Id,
                        ItemId = orderItem.ItemId, // We need item ID but we must not use item object because of the problem with Modified state
                                                   //when we have multiple items with the same id in one transaction.
                        OrderId = orderItem.OrderId,
                        OrderPrice = orderItem.OrderPrice,
                        Quantity = orderItem.Quantity,
                        Size = orderItem.Size
                    });
                    totalPrice += orderItem.OrderPrice * orderItem.Quantity;
                }

                //racunanje finalne cene kroz discount
                totalPrice = totalPrice * (1 - discount);
                order.TotalPrice = totalPrice;
                order.Status = 1;
                if (order.Id > 0)
                {
                    orderRepository.Update(order);
                }
                else
                {
                    orderRepository.Add(order);
                }

                // Now update dbOrder in session with all new information from database.
                dbOrder.Id = order.Id;
                dbOrder.CustomerName = order.CustomerName;
                dbOrder.CustomerEmail = order.CustomerEmail;
                dbOrder.CustomerAdress = order.CustomerAdress;
                dbOrder.CustomerAdress2 = order.CustomerAdress2;
                dbOrder.City = order.City;
                dbOrder.Country = order.Country;
                dbOrder.DiscountCode = order.DiscountCode;
                dbOrder.ZipCode = order.ZipCode;
                dbOrder.Status = order.Status;
                dbOrder.TotalPrice = order.TotalPrice;
                for (int i = 0; i < order.OrderItems.Count; i++)
                {
                    dbOrder.OrderItems[i].Id = order.OrderItems[i].Id;
                    dbOrder.OrderItems[i].OrderId = order.Id;
                }
                return View("Confirm", dbOrder); // TODO: Empty cart and return link for cartorder view.
            }

            //ako je nevalidan modelstate treba vratiti na stranicu za unos podataka
            return PartialView("CartOrderPopup", order);
        }


        [HttpPost]
        public ActionResult RemoveItem(int id)
        {
            // Id je zapravo index orderItema u order.OrderItems listi, a ne njegov id.
            // Index uzimamo zato sto u najvecem broju slucajeva orderItem jos uvek nema id (id = 0) pa ne bismo mogli da ih razlikujemo.
            Order order = Session["UserOrder"] as Order;
            if (order == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (id < 0 || id >= order.OrderItems.Count)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var orderItem = order.OrderItems[id];
            order.OrderItems.RemoveAt(id);
            if (orderItem.Id > 0)
            {
                orderItem = orderRepository.GetOrderItemById(orderItem.Id);
                if (orderItem != null)
                {
                    orderRepository.DeleteOrderItem(orderItem);
                }
            }

            return PartialView("CartOrderPopup", order);


        }

        //promena broja jednog orderItem-a u korpi poziva je changeQuantity funkcija iz revolver.js
        [HttpPost]
        public ActionResult ChangeItemQuantity(int index, int quantity)
        {
            var order = Session["UserOrder"] as Order;
            if (order == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (index < 0 || index >= order.OrderItems.Count || quantity < 0 || quantity > 100)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var orderItem = order.OrderItems[index];
            orderItem.Quantity = quantity;
            if (quantity == 0)
            {
                order.OrderItems.RemoveAt(index);
            }
            //ako postoji orderItem.Id znaci da postoji u order-u i treba ga obrisati ili apdejtovati u order (radi se o izmeni cart nakon checkout-a a pre placanja)
            if (orderItem.Id > 0)
            {
                orderItem = orderRepository.GetOrderItemById(orderItem.Id);
                if (quantity == 0)
                {
                    orderRepository.DeleteOrderItem(orderItem);
                }
                else
                {
                    orderRepository.UpdateOrderItem(orderItem);
                }
            }
            return RedirectToAction("OpenCart");
        }

        [HttpPost]
        public ActionResult Subscribe(string email)
        {
            string message = string.Empty;
            // TODO: prvo proveri da li je email validan, da nije prazan string i da odgovara REGEXU za email. DONE: klijentska validacija - TODO: serverska validacija za email
            // Onda proveravamo da li vec postoji subskripcija sa ovim emailom (i da li je email poslat).
            var subscription = orderRepository.GetSubscriptionByEmail(email);
            if (subscription != null)
            {
                message = "This email is already used.";
            }
            else
            {
                // Kreiramo subskripciju.
                subscription = orderRepository.CreateSubscription(email);

                //TODO: implementirati ovo po kacenju sajta
                try
                {
                    //MailMessage emailMessage = new MailMessage();
                    //SmtpClient smtp = new SmtpClient();
                    //emailMessage.From = new MailAddress("FromMailAddress"); // Iz konfiga.
                    //emailMessage.To.Add(new MailAddress(subscription.Email));
                    //emailMessage.Subject = "Revolve Pullover Discount Code";
                    //emailMessage.IsBodyHtml = true; //to make message body as html
                    //string htmlString = GetEmailHtml(subscription);
                    //emailMessage.Body = htmlString;
                    //smtp.Port = 587; // Iz konfiga.
                    //smtp.Host = "smtp.gmail.com"; // Iz konfiga.
                    //smtp.EnableSsl = true;
                    //smtp.UseDefaultCredentials = false;
                    //smtp.Credentials = new NetworkCredential("FromMailAddress", "password"); // Iz konfiga.
                    //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //smtp.Send(emailMessage);
                    //// Vratiti poruku klijentu.
                    message = "Thank you for subscribing to our newsletter. We sent a message with a unique discount code to your email.";
                }
                catch (Exception)
                {
                    message = "There was an error while sending the email.";
                }
            }
            return PartialView("SubscriptionPopup", message);
        }

        public static string GetEmailHtml(Subscription subscription)
        {
            string messageBody = "<font>The following is the discount code: </font><br><br>";
            string htmlTableStart = "<table style=\"border-collapse:collapse; text-align:center;\" >";
            string htmlTableEnd = "</table>";
            string htmlHeaderRowStart = "<tr style=\"background-color:#6FA1D2; color:#ffffff;\">";
            string htmlHeaderRowEnd = "</tr>";
            string htmlTdStart = "<td style=\" border-color:#5c87b2; border-style:solid; border-width:thin; padding: 5px;\">";
            string htmlTdEnd = "</td>";
            messageBody += htmlTableStart;
            messageBody += htmlHeaderRowStart;
            messageBody += htmlTdStart + subscription.DiscountCode.Code + htmlTdEnd;
            messageBody += htmlHeaderRowEnd;
            messageBody = messageBody + htmlTableEnd;
            return messageBody; // return HTML Table as string from this function  
        }
    }
}