﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Revolver.Models;

namespace Revolver.Controllers
{
    [Authorize]
    public class ImagesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private readonly string[] allowedExtensions = { ".jpeg", ".jpg", ".png", ".gif" };

        // GET: Images
        public ActionResult Index()
        {
            var images = db.Images.Include(i => i.Item).Include(i => i.Story);
            return View(images.ToList());
        }

        // GET: Images/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Image image = db.Images.Include(i => i.Item).Include(i => i.Story).Include(i=>i.Item.Designer).FirstOrDefault(i => i.Id == id);
            if (image == null)
            {
                return HttpNotFound();
            }
            return View(image);
        }

        // GET: Images/Create
        public ActionResult Create()
        {
            ViewBag.ItemId = new SelectList(db.Items, "Id", "Name");
            ViewBag.StoryId = new SelectList(db.Stories, "Id", "Title");
            ViewBag.DesignerId = new SelectList(db.Designers, "Id", "LastName");

            return View();
        }

        // POST: Images/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Url,Name,Description,ItemId,StoryId,DesignerId,IsThumb")] Image image)
        {
            ViewBag.ItemId = new SelectList(db.Items, "Id", "Name", image.ItemId);
            ViewBag.StoryId = new SelectList(db.Stories, "Id", "Title", image.StoryId);
            ViewBag.DesignerId = new SelectList(db.Designers, "Id", "LastName", image.DesignerId);

            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    if (file.ContentLength > 0)
                    {
                        var extension = Path.GetExtension(file.FileName);
                        if (!allowedExtensions.Contains(extension.ToLower()))
                        {
                            ModelState.AddModelError(string.Empty, "File extension not allowed!");
                            return View(image);
                        }
                        var fileName = image.Name;
                        image.Url = Path.Combine("~/Resources", fileName + extension);
                        var fullPath = Server.MapPath(image.Url);
                        if (System.IO.File.Exists(fullPath))
                        {
                            ModelState.AddModelError(string.Empty, "Image with the same name already exists.");
                            return View(image);
                        }

                        file.SaveAs(fullPath);
                        db.Images.Add(image);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(image);
        }

        // GET: Images/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Image image = db.Images.Find(id);
            if (image == null)
            {
                return HttpNotFound();
            }
            ViewBag.Items = new SelectList(db.Items, "Id", "Name", image.ItemId);
            ViewBag.Stories = new SelectList(db.Stories, "Id", "Title", image.StoryId);
            ViewBag.Designers = new SelectList(db.Designers, "Id", "LastName", image.DesignerId);

            return View(image);
        }

        //POST: Images/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Url,Name,Description,ItemId,StoryId,DesignerId,IsThumb")] Image image)
        {

            if (ModelState.IsValid)
            {
                db.Entry(image).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");

            }

            ViewBag.Items = new SelectList(db.Items, "Id", "Name", image.ItemId);
            ViewBag.Stories = new SelectList(db.Stories, "Id", "Title", image.StoryId);
            ViewBag.Designers = new SelectList(db.Designers, "Id", "LastName", image.DesignerId);


            return View(image);
        }


        // GET: Images/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Image image = db.Images.Include(i => i.Item).Include(i => i.Story).Include(i => i.Item.Designer).FirstOrDefault(i => i.Id == id); ;
            if (image == null)
            {
                return HttpNotFound();
            }
            return View(image);
        }

        // POST: Images/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Image image = db.Images.Find(id);
            System.IO.File.Delete(Server.MapPath(image.Url));
            db.Images.Remove(image);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
