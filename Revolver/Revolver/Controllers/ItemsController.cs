﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Revolver.Models;

namespace Revolver.Controllers
{
    [Authorize]
    public class ItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: Items
        public ActionResult Index()
        {
            var items = db.Items.Include(i => i.Designer).Include(i=>i.Images);
            
            ViewBag.CollectionId = new SelectList(Item.CollectionList, "value", "key");

            return View(items.ToList());
        }

        // GET: Items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Include(i => i.Designer).Include(i=>i.Images).FirstOrDefault(i => i.Id == id);
            ViewBag.CollectionId = new SelectList(Item.CollectionList, "value", "key", item.CollectionId);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: Items/Create
        public ActionResult Create()
        {
            ViewBag.DesignerId = new SelectList(db.Designers, "Id", "LastName");
            ViewBag.CollectionId = new SelectList(Item.CollectionList, "value", "key");
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DesignerId,Name,Price,Excerpt,Text,CollectionId,QuantityXS,QuantityS,QuantityM,QuantityL,QuantityXL")] Item item)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DesignerId = new SelectList(db.Designers, "Id", "LastName", item.DesignerId);
            ViewBag.CollectionId = new SelectList(Item.CollectionList, "value", "key", item.CollectionId);
            return View(item);
        }

        // GET: Items/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Include(i=>i.Designer).Include(i=>i.Images).FirstOrDefault(i=>i.Id==id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.DesignerId = new SelectList(db.Designers, "Id", "LastName", item.DesignerId);
            ViewBag.CollectionId = new SelectList(Item.CollectionList, "value", "key", item.CollectionId);
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DesignerId,Name,Price,Excerpt,Text,CollectionId,QuantityXS,QuantityS,QuantityM,QuantityL,QuantityXL")] Item item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DesignerId = new SelectList(db.Designers, "Id", "LastName", item.DesignerId);
            ViewBag.CollectionId = new SelectList(Item.CollectionList, "value", "key", item.CollectionId);
            return View(item);
        }

        // GET: Items/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Include(i => i.Designer).Include(i=>i.Images).FirstOrDefault(i => i.Id == id);
            ViewBag.CollectionId = new SelectList(Item.CollectionList, "value", "key", item.CollectionId);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
