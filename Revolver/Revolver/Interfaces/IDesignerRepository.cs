﻿using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Revolver.Interfaces
{
    public interface IDesignerRepository
    {
        IEnumerable<Designer> GetAll();
        Designer GetById(int id);
        void Add(Designer designer);
        void Update(Designer designer);
        void Delete(Designer designer);
    }
}
