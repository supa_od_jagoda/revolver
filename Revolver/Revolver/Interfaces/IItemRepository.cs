﻿using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Revolver.Interfaces
{
    public interface IItemRepository
    {
        IEnumerable<Item> GetAll();
        Item GetById(int id);
        void Add(Item item);
        void Update(Item item);
        void Delete(Item item);
        void AddImage(Image image);
        Image GetImageById(int id);
        void UpdateImage(Image image);
        void DeleteImage(Image image);
    }
}
