﻿using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Revolver.Interfaces
{
    interface IOrderSubscriptionRepository
    {
        IEnumerable<Order> GetAll();
        Order GetById(int id);
        void Add(Order order);
        void Update(Order order);
        void Delete(Order order);

        void AddOrderItem(OrderItem orderItem);
        void DeleteOrderItem(OrderItem orderItem);

        IEnumerable<DiscountCode> GetAllDiscountCodes();
        DiscountCode GetDiscountCodeById(int id);
        void UpdateDiscountCode(DiscountCode discountCode);
        void DeleteDiscountCode(DiscountCode discountCode);



        IEnumerable<Subscription> GetAllSubscriptions();
        Subscription GetSubscriptionById(int id);
        void AddSubscription(Subscription subscription);
        void UpdateSubscription(Subscription subscription);
        void DeleteSubscription(Subscription subscription);
    }
}
