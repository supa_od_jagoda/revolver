﻿using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Revolver.Interfaces
{
    public interface IStoryRepository
    {
        IEnumerable<Story> GetAll();
        Story GetById(int id);
        void Add(Story story);
        void Update(Story story);
        void Delete(Story story);
        Image GetImageById(int id);
        void AddImage(Image image);
        void UpdateImage(Image image);
        void DeleteImage(Image image);
    }
}