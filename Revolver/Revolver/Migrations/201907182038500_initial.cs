namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Designers", "Name", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Items", "Name", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Images", "Url", c => c.String(nullable: false));
            AlterColumn("dbo.Images", "Name", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.OrderItems", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "CustomerName", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "CustomerEmail", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "CustomerAdress", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "City", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "Country", c => c.String(nullable: false));
            AlterColumn("dbo.Orders", "ZipCode", c => c.String(nullable: false));
            AlterColumn("dbo.Stories", "AuthorName", c => c.String(nullable: false));
            AlterColumn("dbo.Stories", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Stories", "Text", c => c.String(nullable: false));
            AlterColumn("dbo.Subscriptions", "Email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Subscriptions", "Email", c => c.String());
            AlterColumn("dbo.Stories", "Text", c => c.String());
            AlterColumn("dbo.Stories", "Title", c => c.String());
            AlterColumn("dbo.Stories", "AuthorName", c => c.String());
            AlterColumn("dbo.Orders", "ZipCode", c => c.String());
            AlterColumn("dbo.Orders", "Country", c => c.String());
            AlterColumn("dbo.Orders", "City", c => c.String());
            AlterColumn("dbo.Orders", "CustomerAdress", c => c.String());
            AlterColumn("dbo.Orders", "CustomerEmail", c => c.String());
            AlterColumn("dbo.Orders", "CustomerName", c => c.String());
            AlterColumn("dbo.OrderItems", "Name", c => c.String());
            AlterColumn("dbo.Images", "Name", c => c.String());
            AlterColumn("dbo.Images", "Url", c => c.String());
            AlterColumn("dbo.Items", "Name", c => c.String());
            AlterColumn("dbo.Designers", "Name", c => c.String());
        }
    }
}
