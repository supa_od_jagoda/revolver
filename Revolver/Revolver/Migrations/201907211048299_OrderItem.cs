namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Designers", "LastName", c => c.String());
            AddColumn("dbo.OrderItems", "ItemId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderItems", "Size", c => c.String());
            AddColumn("dbo.OrderItems", "Quantity", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderItems", "ItemId");
            AddForeignKey("dbo.OrderItems", "ItemId", "dbo.Items", "Id", cascadeDelete: true);
            DropColumn("dbo.OrderItems", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderItems", "Name", c => c.String(nullable: false));
            DropForeignKey("dbo.OrderItems", "ItemId", "dbo.Items");
            DropIndex("dbo.OrderItems", new[] { "ItemId" });
            DropColumn("dbo.OrderItems", "Quantity");
            DropColumn("dbo.OrderItems", "Size");
            DropColumn("dbo.OrderItems", "ItemId");
            DropColumn("dbo.Designers", "LastName");
        }
    }
}
