namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Order1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DiscountCodes", "Used", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "DiscountCodeId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "DiscountCodeId");
            DropColumn("dbo.DiscountCodes", "Used");
        }
    }
}
