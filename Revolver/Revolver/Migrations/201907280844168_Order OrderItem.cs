namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderOrderItem : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "DiscountCodeId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "DiscountCodeId", c => c.Int(nullable: false));
        }
    }
}
