namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderUserOrderId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "UserOrderId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "UserOrderId");
        }
    }
}
