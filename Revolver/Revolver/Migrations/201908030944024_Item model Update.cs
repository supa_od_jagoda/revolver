namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ItemmodelUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "QuantityXS", c => c.Int(nullable: false));
            AddColumn("dbo.Items", "QuantityS", c => c.Int(nullable: false));
            AddColumn("dbo.Items", "QuantityM", c => c.Int(nullable: false));
            AddColumn("dbo.Items", "QuantityL", c => c.Int(nullable: false));
            AddColumn("dbo.Items", "QuantityXL", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "QuantityXL");
            DropColumn("dbo.Items", "QuantityL");
            DropColumn("dbo.Items", "QuantityM");
            DropColumn("dbo.Items", "QuantityS");
            DropColumn("dbo.Items", "QuantityXS");
        }
    }
}
