namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatemodelImageandDesigner : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Designers", "QuoteLarge", c => c.String());
            AddColumn("dbo.Designers", "QuoteSmall", c => c.String());
            AddColumn("dbo.Images", "DesignerId", c => c.Int());
            CreateIndex("dbo.Images", "DesignerId");
            AddForeignKey("dbo.Images", "DesignerId", "dbo.Designers", "Id");
            DropColumn("dbo.Designers", "Quote");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Designers", "Quote", c => c.String());
            DropForeignKey("dbo.Images", "DesignerId", "dbo.Designers");
            DropIndex("dbo.Images", new[] { "DesignerId" });
            DropColumn("dbo.Images", "DesignerId");
            DropColumn("dbo.Designers", "QuoteSmall");
            DropColumn("dbo.Designers", "QuoteLarge");
        }
    }
}
