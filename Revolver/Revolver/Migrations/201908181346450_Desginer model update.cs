namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Desginermodelupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Designers", "Quote", c => c.String());
            AddColumn("dbo.Designers", "AdditionalText", c => c.String());
            DropColumn("dbo.Designers", "QuoteLarge");
            DropColumn("dbo.Designers", "QuoteSmall");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Designers", "QuoteSmall", c => c.String());
            AddColumn("dbo.Designers", "QuoteLarge", c => c.String());
            DropColumn("dbo.Designers", "AdditionalText");
            DropColumn("dbo.Designers", "Quote");
        }
    }
}
