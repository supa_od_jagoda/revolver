namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class defineiftheimageisathumbnail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Images", "IsThumb", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Images", "IsThumb");
        }
    }
}
