namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class izmena : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Subscriptions", "EmailSent", c => c.Boolean(nullable: false));
            AddColumn("dbo.Subscriptions", "DiscountCodeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Subscriptions", "DiscountCodeId");
            AddForeignKey("dbo.Subscriptions", "DiscountCodeId", "dbo.DiscountCodes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscriptions", "DiscountCodeId", "dbo.DiscountCodes");
            DropIndex("dbo.Subscriptions", new[] { "DiscountCodeId" });
            DropColumn("dbo.Subscriptions", "DiscountCodeId");
            DropColumn("dbo.Subscriptions", "EmailSent");
        }
    }
}
