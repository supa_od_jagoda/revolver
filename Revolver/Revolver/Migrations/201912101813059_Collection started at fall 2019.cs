namespace Revolver.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Collectionstartedatfall2019 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "CollectionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "CollectionId");
        }
    }
}
