namespace Revolver.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Revolver.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Revolver.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Revolver.Models.ApplicationDbContext";
        }

        protected override void Seed(Revolver.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            const string userName = "admin@revolverpullover.com";
            const string password = "peperomia300";
            const string roleName = "Administrator";
            const string phoneNumber = "123456";

            var appUserStore = new UserStore<ApplicationUser>(context);
            var appUserManager = new UserManager<ApplicationUser>(appUserStore);
            var appRoleStore = new RoleStore<IdentityRole>(context);
            var appRoleManager = new RoleManager<IdentityRole>(appRoleStore);

            var user = context.Users.SingleOrDefault(u => u.UserName == userName);
            var role = context.Roles.SingleOrDefault(r => r.Name == roleName);

            if (role == null)
            {
                appRoleManager.CreateAsync(new IdentityRole(roleName)).Wait();
                role = context.Roles.SingleOrDefault(r => r.Name == roleName);
            }
            if (user == null)
            {
                appUserManager.CreateAsync(new ApplicationUser { UserName = userName, PhoneNumber = phoneNumber }, password).Wait();
                user = context.Users.SingleOrDefault(u => u.UserName == userName);
            }

            var userRole = user.Roles.SingleOrDefault(r => r.RoleId == role.Id);

            if (userRole == null)
            {
                appUserManager.AddToRoleAsync(user.Id, roleName).Wait();
            }

        }
    }
}
