﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace Revolver.Models
{
    public class ApplicationHelper
    {
        const string Alphabet = "AG8FOLE2WVTCPY5ZH3NIUDBXSMQK7946";
        const int BitCount = 30;
        const int BitMask = (1 << BitCount / 2) - 1;

        private static uint RoundFunction(uint number)
        {
            return (((number ^ 47894) + 25) << 1) & BitMask;
        }

        private static uint Crypt(uint number)
        {
            uint left = number >> (BitCount / 2);
            uint right = number & BitMask;
            for (int round = 0; round < 10; ++round)
            {
                left = left ^ RoundFunction(right);
                uint temp = left; left = right; right = temp;
            }
            return left | (right << (BitCount / 2));
        }

        public static string GenerateCode(uint number)
        {
            number = Crypt(number);
            StringBuilder b = new StringBuilder();
            for (int i = 0; i < 6; ++i)
            {
                b.Append(Alphabet[(int)number & ((1 << 5) - 1)]);
                number = number >> 5;
            }
            return b.ToString();
        }

        public static uint NumberFromCode(string code)
        {
            uint n = 0;
            for (int i = 0; i < 6; ++i)
            {
                n = n | (((uint)Alphabet.IndexOf(code[i])) << (5 * i));
            }
            return n;
        }

        public static T Setting<T>(string name)
        {
            string value = ConfigurationManager.AppSettings[name];

            if (value == null)
            {
                throw new Exception(String.Format("Could not find setting '{0}',", name));
            }

            return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
        }
    }
}