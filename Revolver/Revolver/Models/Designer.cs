﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class Designer
    {
        public int Id { get; set; }
        [Required]
        [StringLength(maximumLength: 1000, MinimumLength = 1)]
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Biography { get; set; }
        public string Quote{ get; set; }
        public string AdditionalText { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }

        public List<Item> Items { get; set; }
    }
}