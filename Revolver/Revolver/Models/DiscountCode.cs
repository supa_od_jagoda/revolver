﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class DiscountCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public bool Assigned { get; set; }
        public bool Used { get; set; }
        public float Value { get; set; }

    }
}