﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class Image
    {
        public int Id { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        [StringLength(maximumLength: 1000, MinimumLength = 1)]
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ItemId { get; set; }
        [ForeignKey("ItemId")]
        public Item Item { get; set; }
        public int? StoryId { get; set; }
        [ForeignKey("StoryId")]
        public Story Story { get; set; }
        public int? DesignerId { get; set; }
        [ForeignKey("DesignerId")]
        public Designer Designer { get; set; }
        public bool IsThumb { get; set; }


    }
}