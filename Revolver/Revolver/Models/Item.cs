﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class Item
    {
        public int Id { get; set; }
        [Required]
        public int DesignerId { get; set; }
        [ForeignKey("DesignerId")]
        public Designer Designer { get; set; }
        [Required]
        [StringLength(maximumLength: 1000, MinimumLength = 1)]
        public string Name { get; set; }
        public float Price { get; set; }
        public string Excerpt { get; set; }
        public string Text { get; set; }
        public int CollectionId { get; set; }

        [NotMapped]
        public virtual string CollectionName
        {
            get
            {
                var collection = CollectionList.FirstOrDefault(col => col.Value==CollectionId);
                return collection.Key;               
            }
        }


        [NotMapped]
        public virtual string Thumbnail
        {
            get
            {
                var image = Images.FirstOrDefault(im => im.IsThumb);
                if (image != null)
                {
                    return image.Url;
                }
                return " ";
            }
       }


        //integeri za xs, s, m, l, xl
        
        public int QuantityXS { get; set; }
        public int QuantityS { get; set; }
        public int QuantityM { get; set; }
        public int QuantityL { get; set; }
        public int QuantityXL { get; set; }


        //apdtejtovati admin stranicu za iteme
        //ostaviti mogucnost upisivanja 0 kad item-a nema
        //raditi proveru prilikom ucitavanja stranice za poseban pulover, gde ce se blokirati item ciji je kvantitet 0
        //
        //dodati poruku ukoliko item nije dostupan

        public List<Image> Images { get; set; }


        public static readonly List<KeyValuePair<string, int>> CollectionList =
    new List<KeyValuePair<string, int>> { new KeyValuePair<string, int>("Winter", 0), new KeyValuePair<string, int>("Summer", 1) };

    }
}