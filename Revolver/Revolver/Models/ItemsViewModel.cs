﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class ItemsViewModel
    {
        public List<Item> ItemsWinter { get; set; }
        public List<Item> ItemsSpring { get; set; }

        //TODO: implementirati sezonu kao klasu - kad se odluci tacna struktura modela
        //za sada kontruktor razvrstava listu item-a po kolekcijama koje su odredjene preko CollectionId propertija u Item klasi
        public ItemsViewModel(IEnumerable<Item> items)
        {
            ItemsWinter = new List<Item>();
            ItemsSpring = new List<Item>();
            foreach (var item in items)
            {
                if (item.CollectionId == 0)
                {
                    ItemsWinter.Add(item);

                }
                else
                {
                    ItemsSpring.Add(item);
                }
            }

        }
    }
}