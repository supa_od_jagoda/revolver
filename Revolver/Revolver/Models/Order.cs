﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class Order
    {
        public int Id { get; set; }
        [Required]
        public string CustomerName { get; set; }
        [Required]
        public string CustomerEmail { get; set; }
        [Required]
        public string CustomerAdress { get; set; }
        public string CustomerAdress2 { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string ZipCode { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public string DiscountCode { get; set; }
        public int? DiscountCodeId { get; set; }
        public float Discount { get; set; }
        [Required]
        public float TotalPrice { get; set; }
        public string UserOrderId { get; set; }

        public int Status { get; set; }

        public virtual List<OrderItem> OrderItems { get; set; }
    }

    public enum OrderStatus
    {
        Created = 0,
        Confirmed = 1,
        Pending = 2,
        Paid = 3,
        Canceled = 4
    }
}