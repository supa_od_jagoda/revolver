﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class OrderItem
    {
        public int Id { get; set; }

        [Required]
        public int ItemId { get; set; }
        [ForeignKey("ItemId")]
        public Item Item { get; set; }

        [Required]
        public float OrderPrice { get; set; }

        [Required]
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }

        public ItemSize Size { get; set; }
        public int Quantity { get; set; }


    }

    public enum ItemSize
    {
        XS = 0,
        S = 1,
        M = 2,
        L = 3,
        XL = 4
    }
}