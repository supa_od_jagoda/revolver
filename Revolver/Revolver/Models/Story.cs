﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class Story
    {
        public int Id { get; set; }
        [Required]
        public string AuthorName { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Text { get; set; }

        public List<Image> Images { get; set; }

    }
}