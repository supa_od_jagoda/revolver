﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Revolver.Models
{
    public class Subscription
    {
        public int Id { get; set; }
        [Required]
        public string Email { get; set; }
        public bool EmailSent { get; set; }
        [Required]
        public int DiscountCodeId { get; set; }
        [ForeignKey("DiscountCodeId")]
        public DiscountCode DiscountCode { get; set; }
    }
}