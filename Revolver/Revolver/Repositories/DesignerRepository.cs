﻿using Revolver.Interfaces;
using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Revolver.Repositories
{
    public class DesignerRepository : IDisposable, IDesignerRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Designer designer)
        {
            db.Designers.Add(designer);
            db.SaveChanges();
        }

        public void Delete(Designer designer)
        {
            db.Designers.Remove(designer);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Designer> GetAll()
        {
            return db.Designers.Include(d=>d.Items);
        }

        public Designer GetById(int id)
        {
            return db.Designers.Include(d => d.Items).FirstOrDefault(d => d.Id == id);

        }

        public void Update(Designer designer)
        {
            db.Entry(designer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }
    }
}