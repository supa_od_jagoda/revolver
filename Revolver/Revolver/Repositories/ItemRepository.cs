﻿using Revolver.Interfaces;
using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Revolver.Repositories
{
    public class ItemRepository : IDisposable, IItemRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Item item)
        {
            db.Items.Add(item);
            db.SaveChanges();
        }

        public void AddImage(Image image)
        {
            db.Images.Add(image);
            db.SaveChanges();
        }

        public void Delete(Item item)
        {
            db.Items.Remove(item);
            db.SaveChanges();
        }

        public void DeleteImage(Image image)
        {
            db.Images.Remove(image);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Item> GetAll()
        {
            return db.Items.Include(d => d.Images).Include(d=>d.Designer);
        }

        public Item GetById(int id)
        {
            return db.Items.Include(d => d.Images).Include(d=>d.Designer).FirstOrDefault(d => d.Id == id);

        }

        public Image GetImageById(int id)
        {
            return db.Images.FirstOrDefault(i => i.Id == id);
        }

        public void Update(Item item)
        {
            db.Entry(item).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void UpdateImage(Image image)
        {
            db.Entry(image).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }
    }
}