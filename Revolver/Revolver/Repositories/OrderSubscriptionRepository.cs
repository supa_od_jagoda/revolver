﻿using Revolver.Interfaces;
using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Revolver.Repositories
{
    public class OrderSubscriptionRepository : IDisposable, IOrderSubscriptionRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
        }

        public void AddOrderItem(OrderItem orderItem)
        {
            db.OrderItems.Add(orderItem);
            db.SaveChanges();
        }

        public Subscription CreateSubscription(string email)
        {
            Subscription sub = new Subscription
            {
                Email = email
            };           
            int baseNumber = db.DiscountCodes.Count();
            string code = ApplicationHelper.GenerateCode((uint)baseNumber);
            float discount = ApplicationHelper.Setting<float>("DiscountPercentage");
            DiscountCode newCode = new DiscountCode()
            {
                Code = code,
                Value = discount
            };
            newCode = db.DiscountCodes.Add(newCode);
            sub.DiscountCodeId = newCode.Id;
            sub = db.Subscriptions.Add(sub);
            db.SaveChanges();
            sub.DiscountCode = newCode; // Ovo radimo da bismo vratili discount code nazad u kontroler.

            return sub;
        }

        public void AddSubscription(Subscription subscription)
        {
            db.Subscriptions.Add(subscription);
            db.SaveChanges();
        }

        public void Delete(Order order)
        {
            db.Orders.Remove(order);
            db.SaveChanges();
        }

        public void DeleteOrderItem(OrderItem orderItem)
        {
            db.OrderItems.Remove(orderItem);
            db.SaveChanges();
        }

        public void DeleteSubscription(Subscription subscription)
        {
            db.Subscriptions.Remove(subscription);
            db.SaveChanges();
        }

        public void DeleteDiscountCode(DiscountCode discountCode)
        {
            db.DiscountCodes.Remove(discountCode);
            db.SaveChanges();
        }


        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Order> GetAll()
        {
            return db.Orders.Include(d => d.OrderItems);
        }

        public IEnumerable<DiscountCode> GetAllDiscountCodes()
        {
            return db.DiscountCodes;
        }

        public IEnumerable<Subscription> GetAllSubscriptions()
        {
            return db.Subscriptions;

        }

        public Order GetById(int id)
        {
            return db.Orders.Include(d => d.OrderItems).FirstOrDefault(d => d.Id==id);
        }

        public Order GetByUserOrderId(string userOrderId)
        {
            return db.Orders.AsNoTracking().Include(d => d.OrderItems).FirstOrDefault(d => d.UserOrderId == userOrderId);
        }

        public OrderItem GetOrderItemById(int id)
        {
            return db.OrderItems.FirstOrDefault(oi => oi.Id == id);
        }

        public DiscountCode GetDiscountCodeById(int id)
        {
            return db.DiscountCodes.FirstOrDefault(d => d.Id == id);

        }

        public Subscription GetSubscriptionById(int id)
        {
            return db.Subscriptions.FirstOrDefault(d => d.Id == id);

        }

        public Subscription GetSubscriptionByEmail(string email)
        {
            return db.Subscriptions.Include(s => s.DiscountCode).FirstOrDefault(d => d.Email == email);

        }

        public void UpdateOrderItem(OrderItem orderItem)
        {
            db.Entry(orderItem).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Update(Order order)
        {
            
            db.Entry(order).State = EntityState.Modified;

            try
            {
                foreach (var orderItem in order.OrderItems)
                {
                    if (orderItem.Id > 0)
                    {
                        db.Entry(orderItem).State = EntityState.Modified;
                    }
                    else
                    {
                        db.OrderItems.Add(orderItem);
                    }
                }
                if (order.DiscountCodeId.HasValue)
                {
                    var discountCode = db.DiscountCodes.FirstOrDefault(d => d.Id == order.DiscountCodeId);
                    if (discountCode != null)
                    {
                        discountCode.Used = true;
                        db.Entry(discountCode).State = EntityState.Modified;
                    }
                }
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void UpdateSubscription(Subscription subscription)
        {
            db.Entry(subscription).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void UpdateDiscountCode(DiscountCode discountCode)
        {
            db.Entry(discountCode).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

        }

    }
}