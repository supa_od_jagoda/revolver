﻿using Revolver.Interfaces;
using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Revolver.Repositories
{
    public class StoryRepository : IDisposable, IStoryRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Story story)
        {
            db.Stories.Add(story);
            db.SaveChanges();
        }

        public void AddImage(Image image)
        {
            db.Images.Add(image);
            db.SaveChanges();
        }

        public void Delete(Story story)
        {
            db.Stories.Remove(story);
            db.SaveChanges();
        }

        public void DeleteImage(Image image)
        {
            db.Images.Remove(image);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Story> GetAll()
        {
            return db.Stories.Include(d => d.Images).ToList();
        }

        public Story GetById(int id)
        {
            return db.Stories.Include(d => d.Images).FirstOrDefault(d => d.Id == id);
        }

        public Image GetImageById(int id)
        {
            return db.Images.FirstOrDefault(i => i.Id == id);
        }

        public void Update(Story story)
        {
            db.Entry(story).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void UpdateImage(Image image)
        {
            db.Entry(image).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }
    }
}