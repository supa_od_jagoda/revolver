﻿$(document).ready(function () {
    // podaci od interesa
    var classBtnItemAvailable = "btn-item-available";
    var classBtnItemUnavailable = "btn-item-unavailable";
    var classBtnItemSelected = "btn-item-selected";



	$("body").on("click", "#btnAddToCart", addToCart);
	$("body").on("click", "#btnBuyNow", buyNow);
    $("body").on("click", "#removeItem", removeItem);
    $("body").on("click", "#decreaseQuantity", decreaseQuantity);
	$("body").on("click", "#increaseQuantity", increaseQuantity);
	$("body").on("click", "#closeSubscription", closeSubscriptionPopup);
	$("body").on("click", "#cart-button", openCart);


	

	$("body").on("click", "#subscribeButton", emailCheck);

	$("body").on("click", "#openMenu", openMenu);
	$("body").on("click", "#closeMenu", closeMenu);
	$("body").on("click", ".truestory", function (e) {
		e.stopPropagation();
	});


	$("body").on("click", "#btnAddToCart", animateCart);

	function animateCart() {

		var div = $(".r-menu");
		var sup = $(".r-menu .CartQuantity");

		var originalSize = $(div).css("font-size");
		var originalSizeSup = $(sup).css("font-size");

		$(div).animate({ fontSize: "36vw" }, 300);
		$(sup).animate({ fontSize: "12vw" }, 300);

		$(div).animate({ fontSize: originalSize }, 300);
		$(sup).animate({ fontSize: originalSizeSup }, 300);

	}

	$("#formOrderItem").submit(function (e) {
		// sprecavanje default akcije forme
		e.preventDefault();
	});

	function buyNow() {
		
        var itemId = $("#itemId").val();
        var itemSize = $("#itemSize").val();

        var sendData = {
            "itemId": itemId,
            "itemSize": itemSize
        };

        $.ajax({
            url: "/Home/AddToCart",
            type: "POST",
            data: sendData
        })
			.done(function (data) {
				openMenu();
				openCart();
				$(".CartQuantity").html(data.CartNumber);
            })
            .fail(function (data) {
                alert("Desila se greska!");
            });
	}

	function addToCart() {
		var itemId = $("#itemId").val();
		var itemSize = $("#itemSize").val();

		var sendData = {
			"itemId": itemId,
			"itemSize": itemSize
		};

		$.ajax({
			url: "/Home/AddToCart",
			type: "POST",
			data: sendData
		})
			.done(function (data) {
				if (data.CartNumber > 0) {
					$(".CartQuantity").html(data.CartNumber);
				} else {
					$(".CartQuantity").html("");
				}
			})
			.fail(function (data) {
				alert("Desila se greska!");
			});
	}

    function removeItem() {

		var orderItemIndex = this.name;
		console.log(orderItemIndex);

        $.ajax({
            url: "/Home/RemoveItem/" + orderItemIndex,
            type: "POST"
        })
            .done(function (data) {
				$("#cart-dropdown").html(data);
				$("#cart-dropdown").css("display", "block");
				var quantity = $("#HiddenQuantity").html();
				if (quantity === "")
				{
					$("#totalPriceOrder").css("display", "none");
					$("#formCustomerDetails").css("display", "none");
				}
				$(".CartQuantity").html(quantity);
            })
            .fail(function () {
                alert("Desila se greska!!!");

            });
    }

    function decreaseQuantity() {
		var orderItemIndex = this.name;
        $label = $("#itemQuantity[name='" + orderItemIndex + "']");
        var quantity = $label.html();
        if (quantity > 0) {
            quantity--;

            changeQuantity(orderItemIndex, quantity);
        }
    }

    function increaseQuantity() {
        var orderItemIndex = this.name;
        $label = $("#itemQuantity[name='" + orderItemIndex + "']");
        var quantity = $label.html();
        if (quantity < 100) {
            quantity++;
            changeQuantity(orderItemIndex, quantity);
        }
    }

    function changeQuantity(orderItemIndex, quantity) {
        $.ajax({
            url: "/Home/ChangeItemQuantity/",
            data: { "index": orderItemIndex, "quantity": quantity },
            type: "POST"
        })
            .done(function (data) {
				$("#cart-dropdown").html(data);
				$("#cart-dropdown").css("display", "block");
				// U html-u (data) imamo podatak o tome koliko itema imamo u korpi.
				// Cuvamo ga u skivenom spanu koji ima id HiddenQuantity.
				var quantity = $("#HiddenQuantity").html();

				if (quantity === "")
				{
					$("#totalPriceOrder").css("display", "none");
					$("#formCustomerDetails").css("display", "none");
				}

				console.log(quantity);
				// Procitamo taj skiveni quanitity i zalepimo ga za sve relevantne spanova shirom celog dokumenta.
				$(".CartQuantity").html(quantity);
            })
            .fail(function () {
                alert("Desila se greska!!!");

            });
    }




    // Product page
    // Button for size XS
    $("#btnXS").click(function () {
        selectItemSize($(this), "XS");
    });

    $("#btnS").click(function () {
        selectItemSize($(this), "S");
    });

    $("#btnM").click(function () {
        selectItemSize($(this), "M");
    });

    $("#btnL").click(function () {
        selectItemSize($(this), "L");
    });

    $("#btnXL").click(function () {
        selectItemSize($(this), "XL");
    });

    function selectItemSize($btn, size) {
        if ($btn.hasClass(classBtnItemAvailable)) {
            if (!$btn.hasClass(classBtnItemSelected)) {
                $("." + classBtnItemSelected).removeClass(classBtnItemSelected);
                $btn.addClass(classBtnItemSelected);
                $("#itemSize").attr("value", size);
            }
        }
	}

	function openMenu() {
		console.log("pozvan open");
		document.getElementById("menu").style.display = "block";
	}

	function closeMenu() {
		document.getElementById("menu").style.display = "none";
		$("#cart-dropdown").css("display", "none");
		$("#cart-button").css("color", "black");

	}

	function clickcont(id) {
		var productId = id;
		$.ajax({
			url: "/Home/Product/" + productId,
			type: "GET"
		})
			.done(function (data) {
				$("#popup").html(data);
				$("#popup").css("display", "block");
			})
			.fail(function () {
				alert("Desila se greska!!!");

			});
	}


	function emailValidation(email) {
		var emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (email.match(emailFormat)) {
			return true;
		}
		else {
			return false;
		}
	}


	function emailCheck() {
		var email = $("#emailNewsletter").val();
		if (emailValidation(email)) {
			subcribeToNewsletter(email);
		}
		else {
			alert("You have entered an invalid email address!");
		}

	}

	function subcribeToNewsletter(email) {

		$("#emailNewsletter").val("");

		alert(email);

		$.ajax({
			type: "POST",
			url: "/Home/Subscribe",
			data: { "email": email },			
			dataType: "html"

		}).done(function (data) {
			$("#subscriptionPopup").html(data);
			document.getElementById("subscription").style.display = "block";
		}).fail(function (data) {
			alert("There was an error. Try again.");
		});

	}

	function closeSubscriptionPopup() {

		document.getElementById("subscription").style.display = "none";

	}

	//klik na cart dugme na menu otvara cart dropdown
	function openCart() {
		var x = $("#cart-button");
		console.log(x.css("color"));
		//ako je cart-dropdown hidden obojiti u belo, otkriti i pozvati getOrder koji je ajax poziv za preuzimanje cart-a 
		if ($("#cart-dropdown").css("display")==="none") {
			x.css("color", "white");
			$("#cart-dropdown").css("display", "block");

			getOrder();
		//ako je cart-dropdown vidljiv zatvoriti ga i obojiti cart tekst u crno
		} else {
			x.css("color", "black");
			$("#cart-dropdown").css("display", "none");
		}
	}

	//ajax poziv za OpenCart 
	function getOrder() {
		$.ajax({
			url: "/Home/OpenCart/",
			type: "GET"
		})
			.done(function (data) {
				$("#cart-dropdown").html(data);
				var quantity = $("#HiddenQuantity").html();

				//ako je cart prazan ne treba prikazivati formu za podatke o customer-u
				if (quantity === "") {
					$("#totalPriceOrder").css("display", "none");
					$("#formCustomerDetails").css("display", "none");
				}
			})
			.fail(function () {
				alert("Desila se greska!!!");
			});
	}

});



