﻿using Revolver.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Revolver
{
    public static class Helpers
    {
        public static MvcHtmlString CartQuantity(this HtmlHelper helper)
        {
            string quantity = string.Empty;

            HttpContext context = HttpContext.Current;
            var order = context.Session["UserOrder"] as Order;
            if (order != null && order.OrderItems.Count > 0)
            {
                quantity = order.OrderItems.Sum(oi => oi.Quantity).ToString();
            }

            return MvcHtmlString.Create(quantity);                
        }

        public static MvcHtmlString CartPriceBeforeDiscount(this HtmlHelper helper)
        {
            string priceBeforeDiscount = string.Empty;

            float priceBeforeDiscountFloat = 0.0f;

            HttpContext context = HttpContext.Current;
            var order = context.Session["UserOrder"] as Order;
            if (order != null && order.OrderItems.Count > 0)
            {
                foreach(OrderItem orderItem in order.OrderItems)
                {
                    var modelPrice = orderItem.Quantity*orderItem.OrderPrice;
                    priceBeforeDiscountFloat += modelPrice;
                }
                priceBeforeDiscount = priceBeforeDiscountFloat.ToString();
            }

            return MvcHtmlString.Create(priceBeforeDiscount);
        }
    }
}